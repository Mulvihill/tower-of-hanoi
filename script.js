clickCounter = 1;
var selectedTower = document.querySelector('#tower.start');
var newTower = document.querySelector('#tower.interim');
var movingTower = document.querySelector('#tower.start');
moveStart = function () {
    
    selectedTower = document.querySelector('#tower.start');
    clickCounter += 1;
    if (clickCounter % 2 == 0) {
        movingBlock = document.querySelector('#tower.start').lastElementChild;
        if (movingBlock.id != 'rod') {
            
            movingTower = selectedTower;
        }
        else (alert('Empty Tower'))
        return;
    }
    else {
        newTower = selectedTower;
        if (Number(movingBlock.dataset.rank) > Number(newTower.lastElementChild.dataset.rank)) {
            return;
        }
        else {
            newTower.appendChild(movingBlock);
        }
    }
    checkWin();
}




moveInterim = function () {
    selectedTower = document.querySelector('#tower.interim');
    clickCounter += 1;
    if (clickCounter % 2 == 0) {
        movingBlock = selectedTower.lastElementChild;
        if (movingBlock.id != 'rod') {
            movingTower = selectedTower;
        }
        else (alert('Empty Tower'))
        return;
    }
    else {
        newTower = selectedTower;
        if (Number(movingBlock.dataset.rank) > Number(newTower.lastElementChild.dataset.rank)) {
            return;
        }
        else {
            newTower.appendChild(movingBlock);
        }
    }
    checkWin();
}

moveFinish = function () {
    selectedTower = document.querySelector('#tower.finish');
    clickCounter += 1;
    if (clickCounter % 2 == 0) {
        movingBlock = selectedTower.lastElementChild;
        if (movingBlock.id != 'rod') {
            movingTower = selectedTower;
        }
        else (alert('Empty Tower'))
        return;
    }
    else {
        newTower = selectedTower;
        if (Number(movingBlock.dataset.rank) > Number(newTower.lastElementChild.dataset.rank)) {
            return;
        }
        else {
            newTower.appendChild(movingBlock);
        }
        //newTower.appendChild(movingBlock);
    }
    checkWin();
}

checkWin = function () {
    if (document.querySelector('#tower.finish').childElementCount == 5) {
        alert('You win!');
    }
}//

document.querySelector('#tower.start').addEventListener('click', moveStart);
document.querySelector('#tower.interim').addEventListener('click', moveInterim);
document.querySelector('#tower.finish').addEventListener('click', moveFinish);